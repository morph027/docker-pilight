FROM	debian:stretch-slim

RUN	echo "deb http://apt.pilight.org/ stable main" > /etc/apt/sources.list.d/pilight.list

ADD	--chown=_apt:root http://apt.pilight.org/pilight.key /etc/apt/trusted.gpg.d/pilight.asc

RUN	apt-get update \
	&& apt-get install -y \
		pilight \
	&& apt-get autoremove -y \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

CMD 	["/usr/local/sbin/pilight-daemon", "-F"]
